open Base;;

type t = {
    vx : float;
    vy : float;
    vz : float;
  }

let make x y z =
  {
    vx = x;
    vy = y;
    vz = z;
  }

let of_points (p1:Point.t) (p2:Point.t) =
  {
    vx = p2.x -. p1.x;
    vy = p2.y -. p1.y;
    vz = p2.z -. p1.z;
  }

let mul {vx; vy; vz} coeff =
  let open Float.O in
  {
    vx = vx * coeff;
    vy = vy * coeff;
    vz = vz * coeff;
  }

let ( * ) v coeff = mul v coeff

let div {vx; vy; vz} coeff =
  let open Float.O in
  {
    vx = vx / coeff;
    vy = vy / coeff;
    vz = vz / coeff;
  }

let dot {vx = x1; vy = y1; vz = z1} {vx = x2; vy = y2; vz = z2} =
  let open Float.O in
  x1 * x2 + y1 * y2 + z1 * z2

let neg v = mul v (-1.)

let add2 {vx = x1; vy = y1; vz = z1} {vx = x2; vy = y2; vz = z2} =
  let open Float.O in
  {
    vx = x1 + x2;
    vy = y1 + y2;
    vz = z1 + z2;
  }

let sub2 {vx = x1; vy = y1; vz = z1} {vx = x2; vy = y2; vz = z2} =
  let open Float.O in
  {
    vx = x2 - x1;
    vy = y2 - y1;
    vz = z2 - z1;
  }

let length {vx; vy; vz} =
  let open Float.O in
  vx * vx + vy * vy + vz * vz |> Float.sqrt

let sq_length {vx; vy; vz} =
  let open Float.O in
  vx * vx + vy * vy + vz * vz

let unit_vector v =
  div v (length v)

let random min max =
  let open Float.O in
  let z_based_range = max - min in
  {
    vx = (Random.float z_based_range) + min;
    vy = (Random.float z_based_range) + min;
    vz = (Random.float z_based_range) + min;
  }

let rec random_unit_sphere () =
  let open Float.O in
  let v = (random (-1.) 1.) in
  if (sq_length v) < 1.
  then v
  else random_unit_sphere ()
;;


let translate {vx ; vy ; vz} ({x ; y ; z}:Point.t) =
  let open Float.O in
  Point.make (x + vx) (y + vy) (z + vz)
;;


let to_string {vx; vy; vz} = Printf.sprintf "{x=%+f; y=%+f; z=%+f}" vx vy vz
