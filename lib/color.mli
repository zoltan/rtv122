(** Reprensents a RGB color. *)
type t

val make : int -> int -> int -> t
val makef : float -> float -> float -> t

val add2 : t -> t -> t

val mul : t -> float -> t
val idiv : t -> int -> t
val sqrt : t -> t

val to_bytes : t -> bytes
val to_char_list : t -> char list

val black : t
val white : t
val red : t
val blue : t
val green : t

val to_string : t -> string
