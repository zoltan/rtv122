type t = {
    x : float;
    y : float;
    z : float;
  }

val make : float -> float -> float -> t
val distance : t -> t -> float

val to_string : t -> string
