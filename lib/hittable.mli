type t =
  | Sphere of Sphere.t

val hit : t list -> Ray.t -> (float * float) -> Hit_record.t option
