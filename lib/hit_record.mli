type t = {
    point : Point.t;
    normal : Vector.t;
    t : float;
    front_face : bool;
  }

val make : Point.t -> Vector.t -> float -> ray:Ray.t -> t
