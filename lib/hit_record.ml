open Base;;

type t = {
    point : Point.t;
    normal : Vector.t;
    t : float;
    front_face : bool;
  }

let make point outward_normal t ~(ray:Ray.t) =
  let open Float.O in
  let front_face = (Vector.dot ray.direction outward_normal) < 0. in
  let normal = match front_face with
    | true -> outward_normal
    | false -> (Vector.neg outward_normal)
  in
  {
    point;
    normal;
    t;
    front_face;
  }
