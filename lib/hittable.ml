type t =
  | Sphere of Sphere.t

let hit_one obj ray t_minmax =
  match obj with
  | Sphere s -> (Sphere.hit s ray t_minmax)


let hit obj_list ray t_minmax =
  let rec inner_f prev_record obj_list ray (t_min, t_max) =
    match obj_list with
    | hd :: tl -> let record = hit_one hd ray (t_min, t_max) in
                  (match record with
                   | Some r -> inner_f record tl ray (t_min, r.t)
                   | None -> inner_f prev_record tl ray (t_min, t_max))
    | [] -> prev_record
  in
  inner_f None obj_list ray t_minmax
