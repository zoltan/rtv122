open Base;;

type t = {
    center: Point.t;
    radius: float
  }

let make center radius = { center; radius }

let _find_root a half_b sqrtd t_min t_max =
  let open Float.O in
  let nhalf_b = Float.neg half_b in
  let root_minus = (nhalf_b - sqrtd) / a in
  let cmp = Float.between ~low:t_min ~high:t_max in
  if cmp root_minus
  then Some root_minus
  else
    let root_plus = (nhalf_b + sqrtd) / a in
    if (cmp root_plus)
    then Some root_plus
    else None


let hit {center; radius} (ray:Ray.t) (t_min, t_max) =
  let open Float.O in
  let oc = Vector.of_points center ray.origin in
  let a = Vector.sq_length ray.direction in
  let half_b = Vector.dot oc ray.direction in
  let c = (Vector.sq_length oc) - (radius * radius) in
  let discriminant = half_b * half_b - a * c in

  if discriminant < 0.
  then None
  else
    match _find_root a half_b (Float.sqrt discriminant) t_min t_max with
    | Some root -> let hit_p = Ray.at ray root in
                   let outward_normal = (Vector.div (Vector.of_points center hit_p) radius) in
                   Some (Hit_record.make
                           hit_p
                           outward_normal
                           root
                           ~ray)
    | _ -> None
