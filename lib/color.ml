open Base;;

exception Invalid of string

type t = {
    r : float;
    g : float;
    b : float;
  }

let make r g b =
  let r' =
    match (Char.of_int r) with
    | Some _ -> (Float.of_int r) /. 255.
    | None -> raise (Invalid "Bad red value")
  in
  let g' =
    match (Char.of_int g) with
    | Some _ -> (Float.of_int g) /. 255.
    | None -> raise (Invalid "Bad green value")
  in
  let b' =
    match (Char.of_int b) with
    | Some _ -> (Float.of_int b) /. 255.
    | None -> raise (Invalid "Bad blue value")
  in
  { r = r'; g = g'; b = b'; }

let makef r g b = {r; g; b}

let add2 {r = r1; g = g1; b = b1} {r = r2; g = g2; b = b2} =
  let open Float.O in
  {
    r = r1 + r2;
    g = g1 + g2;
    b = b1 + b2;
  }

let mul {r; g; b} coeff =
  let open Float.O in
  if (0. > coeff || coeff > 1.)
  then raise (Invalid ("coeff out of range: " ^ (Float.to_string coeff)))
  else
    {
      r = r * coeff;
      g = g * coeff;
      b = b * coeff;
    }

let idiv {r; g; b} coeff =
  let x = (Float.of_int coeff) in
  if (coeff = 0)
  then raise (Invalid ("coeff is 0"))
  else
    let open Float.O in
    let minmax x =
      if x < 0. then 0.
      else if x > 0.999 then 0.999
      else x
    in
    {
      r = minmax(r / x);
      g = minmax(g / x);
      b = minmax(b / x);
    }

let sqrt {r; g; b} =
  {
    r = Float.sqrt r;
    g = Float.sqrt g;
    b = Float.sqrt b;
  }

let float_to_char x = (x *. 255. |> Int.of_float |> Char.of_int_exn)

let to_bytes {r; g; b} =
  Bytes.init 3 ~f:(fun i -> match i with
                            | 0 -> (r |> float_to_char)
                            | 1 -> (g |> float_to_char)
                            | _ -> (b |> float_to_char))

let to_char_list {r; g; b} =
  [ r |> float_to_char;
    g |> float_to_char;
    b |> float_to_char ]

let black = make 0 0 0
let white = make 255 255 255
let red = make 255 0 0
let green = make 0 255 0
let blue = make 0 0 255

let to_string {r; g; b} = Printf.sprintf "{r: %f; g: %f; b: %f}" r g b
