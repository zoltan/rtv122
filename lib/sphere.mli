type t = {
    center: Point.t;
    radius: float
  }

val make : Point.t -> float -> t
val hit : t -> Ray.t -> (float * float) -> Hit_record.t option
