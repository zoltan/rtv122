type t = {
    origin : Point.t;
    direction : Vector.t;
  }

val make : Point.t -> Vector.t -> t
val at : t -> float -> Point.t

val to_string : t -> string
