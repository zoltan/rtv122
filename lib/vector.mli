type t = {
    vx : float;
    vy : float;
    vz : float;
  }

val make : float -> float -> float -> t
val of_points : Point.t -> Point.t -> t

val ( * ) : t -> float -> t
val mul : t -> float -> t
val div : t -> float -> t
val dot : t -> t -> float
val neg : t -> t

val add2 : t -> t -> t
val sub2 : t -> t -> t

val length : t -> float
val sq_length : t -> float
val unit_vector : t -> t
val random : float -> float -> t
val random_unit_sphere : unit -> t
val translate : t -> Point.t -> Point.t

val to_string : t -> string
