open Core;;

let until value =
  let rec f x acc =
    if x = value
    then acc
    else f (x + 1) ((value - x - 1) :: acc)
  in
  f 0 []


(* let make width height ~f =
 *   let height' = height - 1 in
 *   let ws = List.range 0 width in
 *   let run_line h =
 *     let coords = List.map ws ~f:(fun x -> (h, x)) in
 *     Printf.printf "[%4d / %4d]\n" h height;
 *     Parmap.parmap ~ncores:8 ~keeporder:true (fun (y, x) -> f x y) (Parmap.L coords)
 *   in
 *   let rec loop h accum =
 *     if h < 0
 *     then accum
 *     else loop (h - 1) ( (run_line h) @ accum)
 *   in
 *   loop height' [] *)


let make width height ~f =
  let ws = List.range 0 width in
  let hs = List.range 0 height in
  let coords = List.cartesian_product hs ws in
  Parmap.parmap ~ncores:8 ~keeporder:true (fun (y, x) -> f x y) (Parmap.L coords)

let save colors ~width ~height ~path =
  let rec colors_to_char_list acc = function
    | [] -> acc
    | c :: c' -> colors_to_char_list ((Color.to_char_list c) @ acc) c'
  in
  let bytes = colors_to_char_list [] colors
              |> Bytes.of_char_list
  in
  let img = Rgb24.create_with width height [] bytes in
  Png.save path [] (Images.Rgb24 img)
