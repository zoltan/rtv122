open Core;;

let _ray_maker ~image ~viewport ~origin ~lower_left =
  let (img_width, img_height) = image in
  let (vp_width, vp_height) = viewport in
  let origin_v = Vector.of_points origin origin in
  let horizontal = Vector.make vp_width 0. 0. in
  let vertical = Vector.make 0. vp_height 0. in
  let u_dem = ((img_width - 1) |> Float.of_int) in
  let v_dem = ((img_height - 1) |> Float.of_int) in

  let f w h =
    let u = w /. u_dem in
    let v = h /. v_dem in
    let vv = Vector.mul vertical v in
    let uh = Vector.mul horizontal u  in
    let direction = lower_left
                    |> Vector.add2 uh
                    |> Vector.add2 vv
                    |> Vector.sub2 origin_v
    in
    Ray.make origin direction
  in
  f

let void_color (ray:Ray.t) =
  let base_color = Color.make 127 178 255 in
  let target_color = Color.white in
  let unit_direction = Vector.unit_vector (ray.direction) in
  let open Float.O in
  let t = 0.5 * (unit_direction.vy + 1.) in
  let open Color in
  add2 (mul target_color (1. - t)) (mul base_color t)


let rec _ray_color ray max_depth coeff ~world =
  if max_depth <= 0
  then Color.mul Color.black coeff
  else

    match (Hittable.hit world ray (0., Float.infinity)) with
    | Some record -> (
      let target = (Vector.translate record.normal record.point)
                   |> (Vector.translate (Vector.random_unit_sphere ()))
      in
      let direction = Vector.of_points record.point target in
      let new_ray = Ray.make record.point direction in
      _ray_color new_ray (max_depth - 1) (coeff *. 0.5) ~world
    )
    | _ -> (Color.mul (void_color ray) coeff)


let _make_color w h ~(f_ray:(float -> float -> Ray.t)) ~(world:Hittable.t list) ~max_depth =
    let ray = f_ray w h in
    _ray_color ray max_depth 1. ~world

let _make_antialiasing_steps n =
  let open Float.O in
  let part_len = 1. / (Float.of_int n) in
  let rec f_range start max step accum =
    if start >= max
    then accum
    else
      f_range (start + step) max step (start :: accum)
  in
  let centers = f_range 0. 1. part_len []
                |> List.map ~f:(fun x -> x + (part_len / 2.))
  in
  let rec product l1 l2 =
    match l1, l2 with
    | [], _ | _, [] -> []
    | h1::t1, h2::t2 -> (h1,h2)::(product [h1] t2)@(product t1 l2)
  in
  product centers centers
;;

let _antialiasing w h ~(f_ray:(float -> float -> Ray.t)) ~(world:Hittable.t list) ~sqrt_sample_per_pixel ~max_depth =
  let f_color = _make_color ~f_ray ~world ~max_depth in
  let offsets = _make_antialiasing_steps sqrt_sample_per_pixel in
  let colors = List.map offsets ~f:(fun (x, y) -> f_color
                                                    ((Float.of_int w) -. x)
                                                    ((Float.of_int h) -. y))
  in
  let colors_sum = List.fold colors ~init:Color.black ~f:Color.add2 in
  Color.idiv colors_sum (sqrt_sample_per_pixel * sqrt_sample_per_pixel)
  |> Color.sqrt


let _make_viewport ?(viewport_h=2.0) width height =
  let aspect_ratio = (Float.of_int width) /. (Float.of_int height) in
  let viewport_w = aspect_ratio *. viewport_h in
  (viewport_w, viewport_h)


let render width height =
  let (viewport_w, viewport_h) as viewport = _make_viewport width height in
  let origin = Point.make 0. 0. 0. in
  let focal_lenght = 1. in
  let lower_left = Vector.make
                     (origin.x -. viewport_w /. 2.)
                     (origin.y -. viewport_h /. 2.)
                     (origin.z -. focal_lenght)
  in
  let (world: Hittable.t list) = [
      Sphere (Sphere.make (Point.make 0. 0. (-1.)) 0.5);
      Sphere (Sphere.make (Point.make 0. (-100.5) (-1.)) 100.);
    ]
  in
  let f_ray = _ray_maker ~image:(width, height) ~viewport ~origin ~lower_left in
  Stdio.print_endline (Vector.to_string lower_left);
  Frame.make width height ~f:(_antialiasing ~f_ray ~world ~sqrt_sample_per_pixel:10 ~max_depth:10)
