val until : int -> int list

val make: int -> int -> f:(int -> int -> Color.t) -> Color.t list
val save: Color.t list -> width:int -> height:int -> path:string -> unit
