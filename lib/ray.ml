open Base;;

type t = {
    origin : Point.t;
    direction : Vector.t;
  }

let make origin direction = {
    origin = origin;
    direction = direction;
  }
;;

let at {origin; direction} coeff =
  let tmp = Vector.mul direction coeff in
  Point.make (origin.x +. tmp.vx) (origin.y +. tmp.vy) (origin.z +. tmp.vz)
;;

let to_string {origin; direction} =
  Printf.sprintf "origin: %s - direction: %s" (Point.to_string origin) (Vector.to_string direction)
