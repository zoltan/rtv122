open Base
;;

type t = {
    x : float;
    y : float;
    z : float;
  }

let make x y z = {x = x; y = y; z = z}

let distance {x = x1 ; y = y1 ; z = z1} {x = x2 ; y = y2 ; z = z2} =
  let x_diff = (x2 -. x1) **. 2. in
  let y_diff = (y2 -. y1) **. 2. in
  let z_diff = (z2 -. z1) **. 2. in
  x_diff +. y_diff +. z_diff |> Float.sqrt
;;

let to_string {x; y; z} = Printf.sprintf "{x=%+f; y=%+f; z=%+f}" x y z
