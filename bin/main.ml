open Core;;

let time f =
  let t = Unix.gettimeofday () in
  let res = f () in
  Printf.printf "Execution time: %f seconds\n"
                (Unix.gettimeofday () -. t);
  res
;;

let rendering width height path =
  Rtv122.Raytracing.render width height
  |> Rtv122.Frame.save ~width ~height ~path


let main width height path =
  Random.init 21 ;
  time (fun () -> rendering width height path)
;;

let () =
  let command =
    Command.basic
      ~summary:"Simple raytracer"
      (let%map_open.Command
             width = anon ("width" %: int)
       and height = anon ("height" %: int)
       and outfile = anon ("outfile" %: string)
       in fun() -> main width height outfile)
  in
  Command.run command
;;
